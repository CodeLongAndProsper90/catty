from flask import Flask, request, send_file
import random
import os.path
import time
import os
app = Flask(__name__)

def mix(leng):
	fn = ""
	chars = [i for i in range(33, 128) if i != 92]
	for i in range(leng):
		fn += chr(random.choice(chars))
	if os.path.exists(f"files/{fn}"):
		mix()
	return fn

@app.route('/upload', methods=["POST"])
def upload():
	for file in os.path.listdir('files'):
		if time.time() - os.path.getmtime(f"files/{file}") > 86400:
			os.remove('files/'+file)
	f = request.files['file']
	print(type(f))

	f.save((os.path.join("files", mix(5))))
	return "hello"

@app.route('/')
def root():
	return "todo"

@app.route('/<file>')
def getfile(file):
	return send_file(f'files/{file}')

if __name__ == "__main__":
	app.run('0.0.0.0', debug=True)